<?php
function my_script_enqueue(){
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style( 'custom_style', get_template_directory_uri() . '/assets/css/bootstrap.css');
    //add js
    wp_enqueue_script( 'bootstrap_js',  get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js', array('jquery'), null, true );
    wp_enqueue_script( 'custom_js',  get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), null, true );
} 
?>